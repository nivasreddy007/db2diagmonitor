[Files]
Source: "Z:\home\ssamayoa\odesk\DB2DiagMonitor\runnable\DB2DiagMonitor.jar"; DestDir: "{app}"
Source: "Z:\home\ssamayoa\odesk\DB2DiagMonitor\install\run.bat"; DestDir: "{app}"
Source: "Z:\home\ssamayoa\odesk\DB2DiagMonitor\install\db2.ico"; DestDir: "{app}"

[Setup]
AppName=DB2 Diag Monitor
AppVersion=1.0 Beta
AppCopyright=(c) 2013 - Nivasreddy Inaganti
PrivilegesRequired=none
AllowUNCPath=False
ShowLanguageDialog=no
AppPublisher=Nivasreddy Inaganti
AppPublisherURL=http://db2tutorial.net/
AppContact=nivasreddy.is@gmail.com
MinVersion=0,5.01
AlwaysShowComponentsList=False
ShowComponentSizes=False
DefaultDirName={pf}\DB2 Diag Monitor
DefaultGroupName=DB2 Diag Monitor

[Icons]
Name: "{group}\DB2 Diag Monitor"; Filename: "{app}\run.bat"; WorkingDir: "{app}"; IconFilename: "{app}\db2.ico"; IconIndex: 0
Name: "{group}\{cm:UninstallProgram, DB2 Diag Monitor}"; Filename: "{uninstallexe}"
Name: "{userdesktop}\DB2 Diag Monitor"; Filename: "{app}\run.bat"; WorkingDir: "{app}"; IconFilename: "{app}\db2.ico"; IconIndex: 0
