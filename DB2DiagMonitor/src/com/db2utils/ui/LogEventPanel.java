package com.db2utils.ui;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.db2utils.dto.LogEvent;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class LogEventPanel extends JPanel {
	private static final long serialVersionUID = -1000081185935298087L;
	private LogEvent logEvent;
	private JTextField _id;
	private JTextField _timestamp;
	private JTextField _timezone;
	private JTextField _instancename;
	private JTextField _dbpartitionnum;
	private JTextField _dbname;
	private JTextField _pid;
	private JTextField _processname;
	private JTextField _tid;
	private JTextField _appl_id;
	private JTextField _component;
	private JTextField _function;
	private JTextField _probe;
	private JTextField _msgnum;
	private JTextField _msgseverity;
	private JTextField _msgtype;
	private JTextArea _msg;

	public LogEventPanel() {
		setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));

		JLabel label1 = new JLabel("ID:");
		label1.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label1, "2, 2, right, default");

		_id = new JTextField();
		_id.setFont(new Font("Dialog", Font.BOLD, 12));
		_id.setEditable(false);
		add(_id, "4, 2, fill, default");
		_id.setColumns(10);

		JLabel label3 = new JLabel("Timestamp:");
		label3.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label3, "2, 4, right, default");

		_timestamp = new JTextField();
		_timestamp.setFont(new Font("Dialog", Font.BOLD, 12));
		_timestamp.setEditable(false);
		add(_timestamp, "4, 4, fill, default");
		_timestamp.setColumns(10);

		JLabel label4 = new JLabel("Time zone:");
		label4.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label4, "2, 6, right, default");

		_timezone = new JTextField();
		_timezone.setFont(new Font("Dialog", Font.BOLD, 12));
		_timezone.setEditable(false);
		add(_timezone, "4, 6, fill, default");
		_timezone.setColumns(10);

		JLabel label5 = new JLabel("Instance name:");
		label5.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label5, "2, 8, right, default");

		_instancename = new JTextField();
		_instancename.setFont(new Font("Dialog", Font.BOLD, 12));
		_instancename.setEditable(false);
		add(_instancename, "4, 8, fill, default");
		_instancename.setColumns(10);

		JLabel label6 = new JLabel("Partition #:");
		label6.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label6, "2, 10, right, default");

		_dbpartitionnum = new JTextField();
		_dbpartitionnum.setFont(new Font("Dialog", Font.BOLD, 12));
		_dbpartitionnum.setEditable(false);
		add(_dbpartitionnum, "4, 10, fill, default");
		_dbpartitionnum.setColumns(10);

		JLabel label7 = new JLabel("DB name:");
		label7.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label7, "2, 12, right, default");

		_dbname = new JTextField();
		_dbname.setFont(new Font("Dialog", Font.BOLD, 12));
		_dbname.setEditable(false);
		add(_dbname, "4, 12, fill, default");
		_dbname.setColumns(10);

		JLabel label8 = new JLabel("Process ID:");
		label8.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label8, "2, 14, right, default");

		_pid = new JTextField();
		_pid.setFont(new Font("Dialog", Font.BOLD, 12));
		_pid.setEditable(false);
		add(_pid, "4, 14, fill, default");
		_pid.setColumns(10);

		JLabel label9 = new JLabel("Process name:");
		label9.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label9, "2, 16, right, default");

		_processname = new JTextField();
		_processname.setFont(new Font("Dialog", Font.BOLD, 12));
		_processname.setEditable(false);
		add(_processname, "4, 16, fill, default");
		_processname.setColumns(10);

		JLabel label10 = new JLabel("Thread ID:");
		label10.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label10, "2, 18, right, default");

		_tid = new JTextField();
		_tid.setFont(new Font("Dialog", Font.BOLD, 12));
		_tid.setEditable(false);
		add(_tid, "4, 18, fill, default");
		_tid.setColumns(10);

		JLabel label11 = new JLabel("APPL ID:");
		label11.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label11, "2, 20, right, default");

		_appl_id = new JTextField();
		_appl_id.setFont(new Font("Dialog", Font.BOLD, 12));
		_appl_id.setEditable(false);
		add(_appl_id, "4, 20, fill, default");
		_appl_id.setColumns(10);

		JLabel label12 = new JLabel("Component:");
		label12.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label12, "2, 22, right, default");

		_component = new JTextField();
		_component.setFont(new Font("Dialog", Font.BOLD, 12));
		_component.setEditable(false);
		add(_component, "4, 22, fill, default");
		_component.setColumns(10);

		JLabel label13 = new JLabel("Function:");
		label13.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label13, "2, 24, right, default");

		_function = new JTextField();
		_function.setFont(new Font("Dialog", Font.BOLD, 12));
		_function.setEditable(false);
		add(_function, "4, 24, fill, default");
		_function.setColumns(10);

		JLabel label14 = new JLabel("Probe:");
		label14.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label14, "2, 26, right, default");

		_probe = new JTextField();
		_probe.setFont(new Font("Dialog", Font.BOLD, 12));
		_probe.setEditable(false);
		add(_probe, "4, 26, fill, default");
		_probe.setColumns(10);

		JLabel label15 = new JLabel("MSG #:");
		label15.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label15, "2, 28, right, default");

		_msgnum = new JTextField();
		_msgnum.setFont(new Font("Dialog", Font.BOLD, 12));
		_msgnum.setEditable(false);
		add(_msgnum, "4, 28, fill, default");
		_msgnum.setColumns(10);

		JLabel label151 = new JLabel("MSG type:");
		label151.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label151, "2, 30, right, default");

		_msgtype = new JTextField();
		_msgtype.setEditable(false);
		_msgtype.setFont(new Font("Dialog", Font.BOLD, 12));
		add(_msgtype, "4, 30, fill, default");
		_msgtype.setColumns(10);

		JLabel label16 = new JLabel("MSG severity:");
		label16.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label16, "2, 32, right, default");

		_msgseverity = new JTextField();
		_msgseverity.setFont(new Font("Dialog", Font.BOLD, 12));
		_msgseverity.setEditable(false);
		add(_msgseverity, "4, 32, fill, default");
		_msgseverity.setColumns(10);

		JLabel label17 = new JLabel("MSG:");
		label17.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(label17, "2, 34, right, default");

		_msg = new JTextArea();
		_msg.setEditable(false);
		add(_msg, "4, 34, fill, fill");
	}

	public LogEvent getLogEvent() {
		return logEvent;
	}

	public void setLogEvent(LogEvent logEvent) {
		this.logEvent = logEvent;
		if (logEvent == null) {
			return;
		}
		_id.setText("" + logEvent.getId());
		_timestamp.setText("" + logEvent.getTimestamp_());
		_timezone.setText("" + logEvent.getTimezone());
		_instancename.setText(logEvent.getInstancename());
		_dbpartitionnum.setText("" + logEvent.getDbpartitionnum());
		_dbname.setText(logEvent.getDbname());
		_pid.setText("" + logEvent.getPid());
		_processname.setText(logEvent.getProcessname());
		_tid.setText("" + logEvent.getTid());
		_appl_id.setText(logEvent.getAppl_id());
		_component.setText(logEvent.getComponent());
		_function.setText(logEvent.getFunction());
		_probe.setText("" + logEvent.getProbe());
		_msgnum.setText("" + logEvent.getMsgnum());
		_msgtype.setText(logEvent.getMsgtype());
		_msgseverity.setText(logEvent.getMsgseverity());
		_msg.setText(logEvent.getMsg());

	}
}
