package com.db2utils.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.observablecollections.ObservableCollections;

import com.db2utils.DataAccess;
import com.db2utils.Utils;
import com.db2utils.ui.model.DB2ConnProps;

public class ConfigDialog extends JDialog {
	private static final long serialVersionUID = -4053699281304705061L;
	private JTextField localDbText;
	private JTable connPropsTable;

	private List<DB2ConnProps> connPropsList = ObservableCollections
			.observableList(new ArrayList<DB2ConnProps>());
	private DB2ConnProps selConnProps;

	private MainWindow mainWindow;
	private JButton addDb2ConnBtn;
	private JButton editDb2ConnBtn;
	private JButton deleteDb2ConnBtn;
	private JButton testDb2ConnBtn;

	public ConfigDialog(MainWindow mainWindow) {
		setResizable(false);
		this.mainWindow = mainWindow;

		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Configuration");
		setBounds(100, 100, 700, 550);

		JPanel localDBPanel = new JPanel();
		FlowLayout fl_localDBPanel = (FlowLayout) localDBPanel.getLayout();
		fl_localDBPanel.setAlignment(FlowLayout.LEFT);
		getContentPane().add(localDBPanel, BorderLayout.NORTH);

		JLabel label1 = new JLabel("Local database:");
		localDBPanel.add(label1);

		localDbText = new JTextField();
		localDBPanel.add(localDbText);
		localDbText.setColumns(10);

		JPanel db2ConnPanel = new JPanel();
		db2ConnPanel.setBorder(new TitledBorder(null, " DB2 Connections: ",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(db2ConnPanel, BorderLayout.CENTER);
		db2ConnPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		db2ConnPanel.add(scrollPane, BorderLayout.CENTER);

		connPropsTable = new JTable();
		connPropsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(connPropsTable);

		JPanel db2ConnButtonsPanel = new JPanel();
		FlowLayout fl_db2ConnButtonsPanel = (FlowLayout) db2ConnButtonsPanel
				.getLayout();
		fl_db2ConnButtonsPanel.setAlignment(FlowLayout.LEFT);
		db2ConnPanel.add(db2ConnButtonsPanel, BorderLayout.SOUTH);

		addDb2ConnBtn = new JButton("Add");
		addDb2ConnBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getConnConfigDialog().addNew();
			}
		});
		db2ConnButtonsPanel.add(addDb2ConnBtn);

		editDb2ConnBtn = new JButton("Edit");
		editDb2ConnBtn.setEnabled(false);
		editDb2ConnBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getConnConfigDialog().edit(selConnProps);
			}
		});
		db2ConnButtonsPanel.add(editDb2ConnBtn);

		deleteDb2ConnBtn = new JButton("Delete");
		deleteDb2ConnBtn.setEnabled(false);
		deleteDb2ConnBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connPropsList.remove(selConnProps);
			}
		});
		db2ConnButtonsPanel.add(deleteDb2ConnBtn);

		testDb2ConnBtn = new JButton("Test");
		testDb2ConnBtn.setEnabled(false);
		testDb2ConnBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				testConnection(selConnProps);
			}
		});
		db2ConnButtonsPanel.add(testDb2ConnBtn);

		JPanel buttonsPanel = new JPanel();
		FlowLayout fl_buttonsPanel = (FlowLayout) buttonsPanel.getLayout();
		fl_buttonsPanel.setAlignment(FlowLayout.RIGHT);
		getContentPane().add(buttonsPanel, BorderLayout.SOUTH);

		JButton okBtn = new JButton("OK");
		okBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConfigDialog cd = ConfigDialog.this;
				if (cd.saveConfig()) {
					cd.setVisible(false);
					cd.mainWindow.loadConfig();
					cd.mainWindow.refresh();
					cd.dispose();
				}
			}
		});
		buttonsPanel.add(okBtn);

		JButton cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConfigDialog.this.dispose();
			}
		});
		buttonsPanel.add(cancelBtn);
		initDataBindings();
		loadConfig();

		connPropsTable.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (e.getValueIsAdjusting()) {
							return;
						}
						int index = connPropsTable.getSelectedRow();
						if (index == -1) {
							setSelConnProps(null);
						} else {
							setSelConnProps(connPropsList.get(index));
						}
					}
				});
	}

	private ConnConfigDialog connConfigDialog;

	protected ConnConfigDialog getConnConfigDialog() {
		if (connConfigDialog == null) {
			connConfigDialog = new ConnConfigDialog(this);
			connConfigDialog.setLocationRelativeTo(this);
		}
		return connConfigDialog;
	}

	protected void processException(Exception exception) {
		JOptionPane.showMessageDialog(this,
				Utils.prepareExeceptionMessage(exception),
				"Unexpected exception", JOptionPane.ERROR_MESSAGE);
	}

	protected void loadConfig() {
		File file = new File("./config.properties");
		if (file.exists()) {
			Properties conf = new Properties();
			try {
				InputStream in = new FileInputStream(file);
				try {
					conf.load(in);
				} finally {
					in.close();
				}
				localDbText.setText(conf
						.getProperty(DataAccess.LOCAL_DB_LOCATION));
				int serverNo = 0;
				for (;;) {
					String serverName = conf.getProperty(DataAccess.DB2_NAME
							+ serverNo);
					if (serverName == null) {
						break;
					}
					connPropsList
							.add(new DB2ConnProps(serverName, conf
									.getProperty(DataAccess.DB2_SERVER
											+ serverNo), Integer.parseInt(conf
									.getProperty(
											DataAccess.DB2_PORT + serverNo,
											"50000")), conf
									.getProperty(DataAccess.DB2_DBNAME
											+ serverNo), conf
									.getProperty(DataAccess.DB2_USERNAME
											+ serverNo), conf
									.getProperty(DataAccess.DB2_PASSWORD
											+ serverNo)));
					serverNo++;
				}
			} catch (Exception ex) {
				processException(ex);
			}
		}
	}

	protected boolean saveConfig() {
		try {
			Properties conf = new Properties();
			conf.put(DataAccess.LOCAL_DB_LOCATION, localDbText.getText());
			int serverNo = 0;
			for (DB2ConnProps connProps : connPropsList) {
				conf.put(DataAccess.DB2_NAME + serverNo, connProps.getName());
				conf.put(DataAccess.DB2_SERVER + serverNo,
						connProps.getServer());
				conf.put(DataAccess.DB2_PORT + serverNo,
						"" + connProps.getPort());
				conf.put(DataAccess.DB2_DBNAME + serverNo,
						connProps.getDatabaseName());
				conf.put(DataAccess.DB2_USERNAME + serverNo,
						connProps.getUserName());
				conf.put(DataAccess.DB2_PASSWORD + serverNo,
						connProps.getPassword());
				serverNo++;
			}
			conf.store(new FileOutputStream("./config.properties"), "");
			return true;
		} catch (Exception ex) {
			processException(ex);
		}
		return false;
	}

	public DB2ConnProps getSelConnProps() {
		return selConnProps;
	}

	public void setSelConnProps(DB2ConnProps selConnProps) {
		boolean enable = selConnProps != null;
		editDb2ConnBtn.setEnabled(enable);
		deleteDb2ConnBtn.setEnabled(enable);
		testDb2ConnBtn.setEnabled(enable);
		this.selConnProps = selConnProps;
	}

	public List<DB2ConnProps> getConnPropsList() {
		return connPropsList;
	}

	public void testConnection(DB2ConnProps connProps) {
		try {
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			try {
				String url = MessageFormat.format(
						"jdbc:db2://{0}:{1,number,0}/{2}",
						connProps.getServer(), connProps.getPort(),
						connProps.getDatabaseName());
				Connection con = DriverManager.getConnection(url,
						connProps.getUserName(), connProps.getPassword());
				con.close();
				JOptionPane.showMessageDialog(this, "Connection to "
						+ connProps.getServer() + " was successful!",
						"Success", JOptionPane.INFORMATION_MESSAGE);
			} catch (SQLException ex) {
				JOptionPane.showMessageDialog(
						this,
						"Test connection failed, reason:\n"
								+ Utils.prepareExeceptionMessage(ex),
						"Test connection failed", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception ex) {
			processException(ex);
		}
	}

	protected void initDataBindings() {
		org.jdesktop.swingbinding.JTableBinding<com.db2utils.ui.model.DB2ConnProps, java.util.List<com.db2utils.ui.model.DB2ConnProps>, javax.swing.JTable> jTableBinding = org.jdesktop.swingbinding.SwingBindings
				.createJTableBinding(
						org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ,
						connPropsList, connPropsTable);
		//
		org.jdesktop.beansbinding.BeanProperty<com.db2utils.ui.model.DB2ConnProps, java.lang.String> dB2ConnPropsBeanProperty_3 = org.jdesktop.beansbinding.BeanProperty
				.create("name");
		jTableBinding.addColumnBinding(dB2ConnPropsBeanProperty_3)
				.setColumnName("Name").setEditable(false);
		//
		org.jdesktop.beansbinding.BeanProperty<com.db2utils.ui.model.DB2ConnProps, java.lang.String> dB2ConnPropsBeanProperty_4 = org.jdesktop.beansbinding.BeanProperty
				.create("server");
		jTableBinding.addColumnBinding(dB2ConnPropsBeanProperty_4)
				.setColumnName("Server name or IP").setEditable(false);
		//
		org.jdesktop.beansbinding.BeanProperty<com.db2utils.ui.model.DB2ConnProps, java.lang.Integer> dB2ConnPropsBeanProperty = org.jdesktop.beansbinding.BeanProperty
				.create("port");
		jTableBinding.addColumnBinding(dB2ConnPropsBeanProperty)
				.setColumnName("Port").setEditable(false);
		//
		org.jdesktop.beansbinding.BeanProperty<com.db2utils.ui.model.DB2ConnProps, java.lang.String> dB2ConnPropsBeanProperty_1 = org.jdesktop.beansbinding.BeanProperty
				.create("databaseName");
		jTableBinding.addColumnBinding(dB2ConnPropsBeanProperty_1)
				.setColumnName("Database name").setEditable(false);
		//
		org.jdesktop.beansbinding.BeanProperty<com.db2utils.ui.model.DB2ConnProps, java.lang.String> dB2ConnPropsBeanProperty_2 = org.jdesktop.beansbinding.BeanProperty
				.create("userName");
		jTableBinding.addColumnBinding(dB2ConnPropsBeanProperty_2)
				.setColumnName("User name").setEditable(false);
		//
		jTableBinding.setEditable(false);
		jTableBinding.bind();
	}
}
