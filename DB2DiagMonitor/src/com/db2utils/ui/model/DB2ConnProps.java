package com.db2utils.ui.model;

public class DB2ConnProps extends UIModelBase {
	private String name;
	private String server;
	private int port = 50000;
	private String databaseName;
	private String userName;
	private String password;

	public DB2ConnProps() {
	}

	public DB2ConnProps(String name, String server, int port,
			String databaseName, String userName, String password) {
		super();
		this.name = name;
		this.server = server;
		this.port = port;
		this.databaseName = databaseName;
		this.userName = userName;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		String old = this.name;
		this.name = name;
		firePropertyChange("name", old, name);
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		String old = this.server;
		this.server = server;
		firePropertyChange("server", old, server);
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		int old = this.port;
		this.port = port;
		firePropertyChange("port", old, port);
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		String old = this.databaseName;
		this.databaseName = databaseName;
		firePropertyChange("databaseName", old, databaseName);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		String old = this.userName;
		this.userName = userName;
		firePropertyChange("userName", old, userName);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		String old = this.password;
		this.password = password;
		firePropertyChange("password", old, password);
	}

	public void clear() {
		setName("");
		setServer("");
		setPort(50000);
		setDatabaseName("");
		setUserName("");
		setPassword("");
	}

	public void copyFrom(DB2ConnProps db2ConnProps) {
		setName(db2ConnProps.name);
		setServer(db2ConnProps.server);
		setPort(db2ConnProps.port);
		setDatabaseName(db2ConnProps.databaseName);
		setUserName(db2ConnProps.userName);
		setPassword(db2ConnProps.password);
	}

	@Override
	public DB2ConnProps clone() {
		return new DB2ConnProps(name, server, port, databaseName, userName,
				password);
	}

}
