package com.db2utils.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Bindings;

import com.db2utils.Utils;
import com.db2utils.ui.model.DB2ConnProps;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class ConnConfigDialog extends JDialog {
	private static final long serialVersionUID = -353765417157703567L;
	private JTextField nameText;
	private JTextField serverText;
	private JTextField portText;
	private JTextField userNameText;
	private JTextField passwordText;
	private JTextField dbNameText;

	private DB2ConnProps connProps = new DB2ConnProps();

	private ConfigDialog configDialog;

	private boolean adding;
	private DB2ConnProps editindConnProps;

	public ConnConfigDialog(ConfigDialog configDialog) {
		this.configDialog = configDialog;

		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Connection properties");
		setBounds(100, 100, 460, 290);

		JPanel bottomPanel = new JPanel();
		getContentPane().add(bottomPanel, BorderLayout.SOUTH);
		bottomPanel.setLayout(new BorderLayout(0, 0));

		JPanel testPanel = new JPanel();
		FlowLayout fl_testPanel = (FlowLayout) testPanel.getLayout();
		fl_testPanel.setAlignment(FlowLayout.LEFT);
		bottomPanel.add(testPanel, BorderLayout.WEST);

		JButton testBtn = new JButton("Test");
		testBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!validateData()) {
					return;
				}
				ConnConfigDialog.this.configDialog.testConnection(connProps);
			}
		});
		testPanel.add(testBtn);

		JPanel okCancelPanel = new JPanel();
		FlowLayout fl_okCancelPanel = (FlowLayout) okCancelPanel.getLayout();
		fl_okCancelPanel.setAlignment(FlowLayout.RIGHT);
		bottomPanel.add(okCancelPanel, BorderLayout.EAST);

		JButton okBtn = new JButton("OK");
		okBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!validateData()) {
					return;
				}
				if (adding) {
					ConfigDialog configDialog = ConnConfigDialog.this.configDialog;
					String name = connProps.getName();
					List<DB2ConnProps> list = configDialog.getConnPropsList();
					for (DB2ConnProps cp : list) {
						if (cp.getName().equalsIgnoreCase(name)) {
							JOptionPane
									.showMessageDialog(
											configDialog,
											"Name already exits.\nPlease enter a different one.",
											"Error", JOptionPane.ERROR_MESSAGE);
							nameText.requestFocus();
							return;
						}
					}
					list.add(connProps);
				} else {
					editindConnProps.copyFrom(connProps);
				}
				setVisible(false);
			}
		});
		okCancelPanel.add(okBtn);

		JButton cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		okCancelPanel.add(cancelBtn);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(
				new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"),
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label1 = new JLabel("Name:");
		label1.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel.add(label1, "2, 2, right, default");

		nameText = new JTextField();
		nameText.setFont(new Font("Dialog", Font.BOLD, 12));
		panel.add(nameText, "4, 2, fill, default");
		nameText.setColumns(10);

		JLabel label2 = new JLabel("Server name or IP:");
		label2.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel.add(label2, "2, 4, right, default");

		serverText = new JTextField();
		serverText.setFont(new Font("Dialog", Font.BOLD, 12));
		panel.add(serverText, "4, 4, fill, default");
		serverText.setColumns(10);

		JLabel label3 = new JLabel("Port:");
		label3.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel.add(label3, "2, 6, right, default");

		portText = new JTextField();
		portText.setFont(new Font("Dialog", Font.BOLD, 12));
		panel.add(portText, "4, 6, fill, default");
		portText.setColumns(10);

		JLabel label4 = new JLabel("Database name:");
		label4.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel.add(label4, "2, 8, right, default");

		dbNameText = new JTextField();
		dbNameText.setFont(new Font("Dialog", Font.BOLD, 12));
		panel.add(dbNameText, "4, 8, fill, default");
		dbNameText.setColumns(10);

		JLabel label5 = new JLabel("User name:");
		label5.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel.add(label5, "2, 10, right, default");

		userNameText = new JTextField();
		userNameText.setFont(new Font("Dialog", Font.BOLD, 12));
		panel.add(userNameText, "4, 10, fill, default");
		userNameText.setColumns(10);

		JLabel label6 = new JLabel("Password:");
		label6.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel.add(label6, "2, 12, right, default");

		passwordText = new JTextField();
		passwordText.setFont(new Font("Dialog", Font.BOLD, 12));
		panel.add(passwordText, "4, 12, fill, default");
		initDataBindings();

	}

	public void addNew() {
		setTitle("New DB2 server connection");
		adding = true;
		connProps.clear();
		nameText.setEnabled(true);
		setVisible(true);
		nameText.requestFocus();
	}

	public void edit(DB2ConnProps connProps) {
		setTitle("Editing DB2 server connection");
		adding = false;
		editindConnProps = connProps;
		this.connProps.copyFrom(connProps);
		nameText.setEnabled(false);
		setVisible(true);
		serverText.requestFocus();
	}

	protected boolean validateData() {
		String msg = null;
		Component comp = nameText;
		if (Utils.isEmpty(connProps.getName())) {
			msg = "Please enter name.";
		}
		if (msg == null && connProps.getName().length() > 128) {
			msg = "Name too long. Please enter up to 128 chars.";
		}
		if (msg == null && Utils.isEmpty(connProps.getServer())) {
			comp = serverText;
			msg = "Please enter server name or ip.";
		}
		if (msg == null && connProps.getPort() <= 0) {
			comp = portText;
			msg = "Please enter a valid port number.";
		}
		if (msg == null && Utils.isEmpty(connProps.getDatabaseName())) {
			comp = dbNameText;
			msg = "Please enter database name.";
		}
		if (msg == null && Utils.isEmpty(connProps.getUserName())) {
			comp = userNameText;
			msg = "Please enter user name.";
		}
		if (msg == null && Utils.isEmpty(connProps.getPassword())) {
			comp = passwordText;
			msg = "Please enter the password.";
		}
		if (msg != null) {
			JOptionPane.showMessageDialog(this, msg, "Error",
					JOptionPane.ERROR_MESSAGE);
			comp.requestFocus();
		}
		return msg == null;
	}

	protected void initDataBindings() {
		BeanProperty<DB2ConnProps, String> dB2ConnPropsBeanProperty = BeanProperty
				.create("name");
		BeanProperty<JTextField, String> jTextFieldBeanProperty = BeanProperty
				.create("text");
		AutoBinding<DB2ConnProps, String, JTextField, String> autoBinding = Bindings
				.createAutoBinding(UpdateStrategy.READ_WRITE, connProps,
						dB2ConnPropsBeanProperty, nameText,
						jTextFieldBeanProperty);
		autoBinding.bind();
		//
		BeanProperty<DB2ConnProps, String> dB2ConnPropsBeanProperty_1 = BeanProperty
				.create("server");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_1 = BeanProperty
				.create("text");
		AutoBinding<DB2ConnProps, String, JTextField, String> autoBinding_1 = Bindings
				.createAutoBinding(UpdateStrategy.READ_WRITE, connProps,
						dB2ConnPropsBeanProperty_1, serverText,
						jTextFieldBeanProperty_1);
		autoBinding_1.bind();
		//
		BeanProperty<DB2ConnProps, Integer> dB2ConnPropsBeanProperty_2 = BeanProperty
				.create("port");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_2 = BeanProperty
				.create("text");
		AutoBinding<DB2ConnProps, Integer, JTextField, String> autoBinding_2 = Bindings
				.createAutoBinding(UpdateStrategy.READ_WRITE, connProps,
						dB2ConnPropsBeanProperty_2, portText,
						jTextFieldBeanProperty_2);
		autoBinding_2.bind();
		//
		BeanProperty<DB2ConnProps, String> dB2ConnPropsBeanProperty_3 = BeanProperty
				.create("databaseName");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_3 = BeanProperty
				.create("text");
		AutoBinding<DB2ConnProps, String, JTextField, String> autoBinding_3 = Bindings
				.createAutoBinding(UpdateStrategy.READ_WRITE, connProps,
						dB2ConnPropsBeanProperty_3, dbNameText,
						jTextFieldBeanProperty_3);
		autoBinding_3.bind();
		//
		BeanProperty<DB2ConnProps, String> dB2ConnPropsBeanProperty_4 = BeanProperty
				.create("userName");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_4 = BeanProperty
				.create("text");
		AutoBinding<DB2ConnProps, String, JTextField, String> autoBinding_4 = Bindings
				.createAutoBinding(UpdateStrategy.READ_WRITE, connProps,
						dB2ConnPropsBeanProperty_4, userNameText,
						jTextFieldBeanProperty_4);
		autoBinding_4.bind();
		//
		BeanProperty<DB2ConnProps, String> dB2ConnPropsBeanProperty_5 = BeanProperty
				.create("password");
		BeanProperty<JTextField, String> jTextFieldBeanProperty_5 = BeanProperty
				.create("text");
		AutoBinding<DB2ConnProps, String, JTextField, String> autoBinding_5 = Bindings
				.createAutoBinding(UpdateStrategy.READ_WRITE, connProps,
						dB2ConnPropsBeanProperty_5, passwordText,
						jTextFieldBeanProperty_5);
		autoBinding_5.bind();
	}
}
