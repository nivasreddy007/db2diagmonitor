package com.db2utils.ui;

import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.Hour;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;

import com.db2utils.DataAccess;
import com.db2utils.dto.LogEventSummary;

public class MsgsChartPanel extends JPanel {
	private static final long serialVersionUID = -7683894878453064111L;

	private static final String VALUE_AXIS_LABEL = "Messages";

	public enum ChartType {
		LAST_24_HOURS("Last 24 hours", "Hour"), //
		LAST_WEEK("Last week"), //
		LAST_MONTH("Last month");

		private String title;
		private String unit = "Date";

		ChartType(String title) {
			this.title = title;
		}

		ChartType(String title, String unit) {
			this.title = title;
			this.unit = unit;
		}

		public String getTitle() {
			return title;
		}

		public String getUnit() {
			return unit;
		}
	}

	public static class LogEventSummaryDataItem extends TimeSeriesDataItem {
		private static final long serialVersionUID = 2771257721276258585L;

		private LogEventSummary logEventSummary;

		public LogEventSummaryDataItem(RegularTimePeriod period, Number value,
				LogEventSummary logEventSummary) {
			super(period, value);
			this.logEventSummary = logEventSummary;
		}

		public LogEventSummary getLogEventSummary() {
			return logEventSummary;
		}
	}

	private ChartType chartType;
	private ChartPanel chartPanel;
	private JFreeChart chart;
	private DataAccess dataAccess;

	public MsgsChartPanel(ChartType chartType) {
		this.chartType = chartType;
		setLayout(new BorderLayout(0, 0));
		chartPanel = new ChartPanel(chart);
		ChartMouseListener listener = new ChartMouseListener() {

			@Override
			public void chartMouseMoved(ChartMouseEvent event) {
			}

			@Override
			public void chartMouseClicked(ChartMouseEvent event) {
				Object o = event.getEntity();
				if (o instanceof XYItemEntity) {
					XYItemEntity itemEntity = (XYItemEntity) o;
					TimeSeriesCollection seriesCollection = (TimeSeriesCollection) itemEntity
							.getDataset();
					TimeSeries series = seriesCollection.getSeries(itemEntity
							.getSeriesIndex());
					TimeSeriesDataItem item = series.getDataItem(itemEntity
							.getItem());
					LogEventSummary logEventSummary = ((LogEventSummaryDataItem) item)
							.getLogEventSummary();
					LogEventsDialog logEventsDialog = new LogEventsDialog();
					logEventsDialog.setTitle("Log events, server: "
							+ logEventSummary.getServerName() + ", severity: "
							+ logEventSummary.getSeverity());
					logEventsDialog.setLogEvents(logEventSummary.getEvents());
					logEventsDialog.setLocationRelativeTo(MsgsChartPanel.this);
					logEventsDialog.setVisible(true);
				}
			}
		};
		chartPanel.addChartMouseListener(listener);
		add(chartPanel, BorderLayout.CENTER);
	}

	public String getTitle() {
		return chartType.getTitle();
	}

	public void setDataAccess(DataAccess dataAccess) {
		this.dataAccess = dataAccess;
	}

	private Map<String, Map<String, List<LogEventSummary>>> logEventSummaries;

	public void loadEvents() throws Exception {
		setVisible(false);
		switch (chartType) {
		case LAST_24_HOURS:
			logEventSummaries = dataAccess.getLogEventSummariesLast24hrs();
			break;
		case LAST_WEEK:
			logEventSummaries = dataAccess.getLogEventSummariesLastWeek();
			break;
		case LAST_MONTH:
			logEventSummaries = dataAccess.getLogEventSummariesLastMonth();
			break;
		}
		TimeSeriesCollection seriesCollection = new TimeSeriesCollection();
		for (Entry<String, Map<String, List<LogEventSummary>>> e0 : logEventSummaries
				.entrySet()) {
			String serverName = e0.getKey();
			for (Entry<String, List<LogEventSummary>> e1 : e0.getValue()
					.entrySet()) {
				String severity = e1.getKey();
				TimeSeries series = new TimeSeries(serverName + " / "
						+ severity);
				for (LogEventSummary logEventSummary : e1.getValue()) {
					RegularTimePeriod period;
					if (chartType == ChartType.LAST_24_HOURS) {
						period = new Hour(logEventSummary.getDate());
					} else {
						period = new Day(logEventSummary.getDate());
					}
					series.add(new LogEventSummaryDataItem(period,
							logEventSummary.getCount(), logEventSummary));
				}
				seriesCollection.addSeries(series);
			}
		}
		chart = ChartFactory.createTimeSeriesChart(getTitle(),
				chartType.getUnit(), VALUE_AXIS_LABEL, seriesCollection, true,
				true, false);

		XYPlot xyPlot = (XYPlot) chart.getPlot();

		DateAxis dateAxis = (DateAxis) xyPlot.getDomainAxis();
		DateTickUnit tickUnit;
		if (chartType == ChartType.LAST_24_HOURS) {
			tickUnit = new DateTickUnit(DateTickUnitType.HOUR, 1,
					new SimpleDateFormat("HH"));
		} else {
			tickUnit = new DateTickUnit(DateTickUnitType.DAY, 1,
					new SimpleDateFormat("MMM dd"));
		}
		dateAxis.setTickUnit(tickUnit);
		dateAxis.setVerticalTickLabels(true);
		XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) xyPlot
				.getRenderer();
		renderer.setBaseShapesVisible(true);
		NumberAxis numberAxis = (NumberAxis) xyPlot.getRangeAxis();
		numberAxis.setAutoRangeIncludesZero(true);
		chartPanel.setChart(chart);
		setVisible(true);
	}
}
