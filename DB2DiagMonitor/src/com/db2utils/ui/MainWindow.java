package com.db2utils.ui;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

import com.db2utils.DataAccess;
import com.db2utils.Utils;
import com.db2utils.ui.MsgsChartPanel.ChartType;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = 481882684866459863L;

	private DataAccess dataAccess;

	private JPanel contentPane;
	private JTabbedPane tabs;
	private MsgsChartPanel last24HoursPanel;
	private MsgsChartPanel lastWeekPanel;
	private MsgsChartPanel lastMonthPanel;
	private JButton refreshBtn;

	private Desktop desktop = Desktop.isDesktopSupported() ? Desktop
			.getDesktop() : null;

	public static void main(String[] args) throws Exception {
		// Set L&F
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			if ("Nimbus".equals(info.getName())) {
				UIManager.setLookAndFeel(info.getClassName());
				break;
			}
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
					frame.loadConfig();
					frame.loadEvents();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainWindow() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (dataAccess != null) {
					dataAccess.done();
				}
			}
		});
		setTitle("DB2 DiagMonitor - BETA");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JMenuBar menuBar = new JMenuBar();
		contentPane.add(menuBar, BorderLayout.NORTH);

		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		JMenuItem confItem = new JMenuItem("Configure...");
		confItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConfigDialog configDialog = new ConfigDialog(MainWindow.this);
				configDialog.setLocationRelativeTo(MainWindow.this);
				configDialog.setVisible(true);
			}
		});
		fileMenu.add(confItem);

		JMenuItem blogItem = new JMenuItem("DB Blog...");
		blogItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (desktop.isSupported(Desktop.Action.BROWSE)) {
					try {
						desktop.browse((new URL("http://db2tutorial.net/"))
								.toURI());
					} catch (Exception ex) {
						processException(ex);
					}
				}
			}
		});
		blogItem.setEnabled(desktop != null);
		fileMenu.add(blogItem);

		fileMenu.addSeparator();

		JMenuItem exitIem = new JMenuItem("Exit");
		exitIem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainWindow.this.dispose();
				System.exit(0);
			}
		});
		fileMenu.add(exitIem);

		tabs = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabs, BorderLayout.CENTER);

		last24HoursPanel = new MsgsChartPanel(ChartType.LAST_24_HOURS);
		tabs.add(last24HoursPanel.getTitle(), last24HoursPanel);

		lastWeekPanel = new MsgsChartPanel(ChartType.LAST_WEEK);
		tabs.add(lastWeekPanel.getTitle(), lastWeekPanel);

		lastMonthPanel = new MsgsChartPanel(ChartType.LAST_MONTH);
		tabs.add(lastMonthPanel.getTitle(), lastMonthPanel);

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel, BorderLayout.SOUTH);

		refreshBtn = new JButton("Refresh");
		refreshBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refresh();
			}
		});
		panel.add(refreshBtn);
	}

	protected void refresh() {
		loadEvents();
		tabs.setSelectedIndex(0);
	}

	protected void processException(Exception exception) {
		JOptionPane.showMessageDialog(this,
				Utils.prepareExeceptionMessage(exception),
				"Unexpected exception", JOptionPane.ERROR_MESSAGE);
	}

	public DataAccess getDataAccess() {
		return dataAccess;
	}

	protected void loadConfig() {
		File file = new File("./config.properties");
		if (file.exists()) {
			Properties conf = new Properties();
			try {
				InputStream in = new FileInputStream(file);
				try {
					conf.load(in);
					if (dataAccess != null) {
						dataAccess.done();
					}
					dataAccess = new DataAccess(conf);
					last24HoursPanel.setDataAccess(dataAccess);
					lastWeekPanel.setDataAccess(dataAccess);
					lastMonthPanel.setDataAccess(dataAccess);
					dataAccess.init();
				} finally {
					in.close();
				}
			} catch (Exception ex) {
				processException(ex);
			}
		} else {
			JOptionPane
					.showMessageDialog(
							this,
							"No configuration file found, use \"Configure\" in \"File menu\".",
							"Warning", JOptionPane.WARNING_MESSAGE);
		}
	}

	private WaitFrame waitFrame;

	protected WaitFrame getWaitFrame(String title) {
		if (waitFrame == null) {
			waitFrame = new WaitFrame();
		}
		if (title != null) {
			waitFrame.setTitle(title);
		}
		return waitFrame;
	}

	protected WaitFrame getWaitFrame() {
		return getWaitFrame(null);
	}

	protected void loadEvents() {
		refreshBtn.setEnabled(false);
		if (dataAccess == null) {
			return;
		}
		getWaitFrame("Loading data...").setLocationRelativeTo(this);
		getWaitFrame().setVisible(true);
		this.setEnabled(false);
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
			Exception exception;
			List<String> result;

			@Override
			protected Void doInBackground() throws Exception {
				try {
					result = dataAccess.getLogEventsFromDB2Servers();
					last24HoursPanel.loadEvents();
					lastWeekPanel.loadEvents();
					lastMonthPanel.loadEvents();
				} catch (Exception ex) {
					exception = ex;
				}
				return null;
			}

			@Override
			public void done() {
				MainWindow.this.setEnabled(true);
				getWaitFrame().setVisible(false);
				if (exception != null) {
					processException(exception);
				}
				if (result != null && result.size() > 0) {
					StringBuilder sb = new StringBuilder();
					for (String s : result) {
						List<String> lines = Utils.splitText(s, " ,;.\t", 50);
						for (String line : lines) {
							sb.append(line);
							sb.append('\n');
						}
					}
					JOptionPane.showMessageDialog(MainWindow.this,
							sb.toString(), "Errors",
							JOptionPane.WARNING_MESSAGE);
				} else {
					refreshBtn.setEnabled(true);
				}
			}
		};
		worker.execute();
	}

}
