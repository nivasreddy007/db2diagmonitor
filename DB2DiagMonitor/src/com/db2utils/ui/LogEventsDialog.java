package com.db2utils.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.observablecollections.ObservableCollections;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import com.db2utils.dto.LogEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Bindings;

public class LogEventsDialog extends JDialog {
	private static final long serialVersionUID = -4515364903937722885L;

	 List<LogEvent> logEvents = ObservableCollections.observableList(new
	 ArrayList<LogEvent>());
	 private JTable logEventsTable;
	 private LogEventPanel logEventPanel;
	// private JTable logEventsTable;

	public LogEventsDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Log events");
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 898, 570);
		getContentPane().setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane = new JSplitPane();
		getContentPane().add(splitPane, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();
		splitPane.setLeftComponent(scrollPane);

		logEventsTable = new JTable();
		scrollPane.setViewportView(logEventsTable);

		logEventPanel = new LogEventPanel();
		splitPane.setRightComponent(logEventPanel);
		splitPane.setDividerLocation(175);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton closeBtn = new JButton("Close");
		closeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogEventsDialog.this.dispose();
			}
		});
		buttonPane.add(closeBtn);
		initDataBindings();
	}
	
	public void setLogEvents(List<LogEvent> logEvents) {
		this.logEvents.clear();
		this.logEvents.addAll(logEvents);
	}
	protected void initDataBindings() {
		JTableBinding<LogEvent, List<LogEvent>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, logEvents, logEventsTable);
		//
		BeanProperty<LogEvent, Timestamp> logEventBeanProperty = BeanProperty.create("timestamp_");
		jTableBinding.addColumnBinding(logEventBeanProperty).setColumnName("Timestamp");
		//
		jTableBinding.setEditable(false);
		jTableBinding.bind();
		//
		BeanProperty<LogEventPanel, LogEvent> logEventPanelBeanProperty = BeanProperty.create("logEvent");
		BeanProperty<JTable, LogEvent> jTableBeanProperty = BeanProperty.create("selectedElement");
		AutoBinding<LogEventPanel, LogEvent, JTable, LogEvent> autoBinding = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, logEventPanel, logEventPanelBeanProperty, logEventsTable, jTableBeanProperty);
		autoBinding.bind();
	}
}
