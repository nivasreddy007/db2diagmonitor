package com.db2utils.ui;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class WaitFrame extends JFrame {
	private static final long serialVersionUID = -5308892615751942088L;

	private JPanel contentPane;

	public WaitFrame() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowIconified(WindowEvent e) {
				WaitFrame.this.setState(JFrame.NORMAL);
			}
		});
		setAlwaysOnTop(true);
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		setTitle("Processing...");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		getContentPane().setMaximumSize(new Dimension(0, 0));
		setResizable(false);
		setBounds(100, 100, 301, 89);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblPleaseWait = new JLabel("Please wait...");
		lblPleaseWait.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblPleaseWait, BorderLayout.CENTER);
	}

}
