package com.db2utils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.db2utils.dto.LogEvent;
import com.db2utils.dto.LogEventSummary;
import com.db2utils.dto.LogEventSummary.SummaryBy;

public class DataAccess {

	public static final String DB2_NAME = "db2.name.";
	public static final String DB2_SERVER = "db2.url.";
	public static final String DB2_PORT = "db2.port.";
	public static final String DB2_DBNAME = "db2.dbname.";
	public static final String DB2_USERNAME = "db2.username.";
	public static final String DB2_PASSWORD = "db2.password.";
	public static final String LOCAL_DB_LOCATION = "local.db_location";

	protected static class DB2ServerInfo {
		private String serverName;
		private String url;
		private String userName;
		private String passWord;

		public DB2ServerInfo(String serverName, String url, String userName,
				String passWord) {
			this.serverName = serverName;
			this.url = url;
			this.userName = userName;
			this.passWord = passWord;
		}

		public String getServerName() {
			return serverName;
		}

		public String getUrl() {
			return url;
		}

		public String getUserName() {
			return userName;
		}

		public String getPassWord() {
			return passWord;
		}
	}

	private List<DB2ServerInfo> db2ServersInfo = new ArrayList<DB2ServerInfo>();
	private String localDbLocation;
	private Connection localDbConnection;

	public DataAccess(Properties conf) {
		int serverNo = 0;
		for (;;) {
			String serverName = conf.getProperty(DB2_NAME + serverNo);
			if (serverName == null) {
				break;
			}
			db2ServersInfo.add(new DB2ServerInfo(serverName, MessageFormat
					.format("jdbc:db2://{0}:{1}/{2}",
							conf.getProperty(DB2_SERVER + serverNo),
							conf.getProperty(DB2_PORT + serverNo, "50000"),
							conf.getProperty(DB2_DBNAME + serverNo)), conf
					.getProperty(DB2_USERNAME + serverNo), conf
					.getProperty(DB2_PASSWORD + serverNo)));
			serverNo++;
		}
		localDbLocation = conf.getProperty(LOCAL_DB_LOCATION);
		if (db2ServersInfo.isEmpty()) {
			throw new IllegalArgumentException(
					"No DB2 servers info in configuration.");
		}
		if (localDbLocation == null) {
			throw new IllegalArgumentException(
					"No local db location in configuration.");
		}
	}

	public void init() throws ClassNotFoundException, SQLException {
		Class.forName("org.hsqldb.jdbc.JDBCDriver");
		Class.forName("com.ibm.db2.jcc.DB2Driver");
		localDbConnection = DriverManager.getConnection(
				MessageFormat.format("jdbc:hsqldb:file:{0}", localDbLocation),
				"sa", "");
		if (!logTableExists(localDbConnection)) {
			createLogTable(localDbConnection);
		}
	}

	public void done() {
		try {
			localDbConnection.close();
		} catch (Exception ex) {
			// Swallow...
		}
	}

	public List<String> getServerNames() {
		List<String> result = new ArrayList<String>();
		for (DB2ServerInfo serverInfo : db2ServersInfo) {
			result.add(serverInfo.getServerName());
		}
		return result;
	}

	/**
	 * <p>
	 * Gets log events from DB2 servers.
	 * </p>
	 * 
	 * @return A list of errors encountered. If empty no error occured.
	 */
	public List<String> getLogEventsFromDB2Servers() {
		List<String> result = new ArrayList<String>();
		for (DB2ServerInfo serverInfo : db2ServersInfo) {
			String serverName = serverInfo.getServerName();
			try {
				Connection con = DriverManager.getConnection(
						serverInfo.getUrl(), serverInfo.getUserName(),
						serverInfo.getPassWord());
				try {
					getLogEventsFromDB2(serverName, con, localDbConnection);
				} finally {
					con.close();
				}
			} catch (Exception ex) {
				result.add(MessageFormat.format("{0} => {1}: {2}", serverName,
						ex.getClass().getSimpleName(), ex.getMessage()));
			}
		}
		return result;
	}

	public Map<String, Map<String, List<LogEventSummary>>> getLogEventSummariesLast24hrs()
			throws SQLException {
		Date toDate = new Date();
		Date fromDate = Utils.addHours(toDate, -24);
		return getLogEventSummaries(fromDate, toDate, SummaryBy.HOUR);
	}

	public Map<String, Map<String, List<LogEventSummary>>> getLogEventSummariesLastWeek()
			throws SQLException {
		Date toDate = new Date();
		Date fromDate = Utils.addDays(toDate, -7);
		return getLogEventSummaries(fromDate, toDate, SummaryBy.DAY);
	}

	public Map<String, Map<String, List<LogEventSummary>>> getLogEventSummariesLastMonth()
			throws SQLException {
		Date toDate = new Date();
		Date fromDate = Utils.addMonths(toDate, -1);
		return getLogEventSummaries(fromDate, toDate, SummaryBy.DAY);
	}

	static final String[] SEVERITIES = { "C", "E", "W", "I" };

	/**
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param summaryBy
	 * @return &lt;serverName, &lt;severity, logEventSummaries&gt;&gt;
	 * @throws SQLException
	 */
	protected Map<String, Map<String, List<LogEventSummary>>> getLogEventSummaries(
			Date fromDate, Date toDate, SummaryBy summaryBy)
			throws SQLException {
		Map<String, Map<String, List<LogEventSummary>>> result = new LinkedHashMap<String, Map<String, List<LogEventSummary>>>();
		for (String serverName : getServerNames()) {
			Map<String, List<LogEventSummary>> logEvenSummaryBySeverity = new LinkedHashMap<String, List<LogEventSummary>>();
			for (String severity : SEVERITIES) {
				List<LogEventSummary> logEventSummaries = getLogEventSummaries(
						serverName, fromDate, toDate, summaryBy, severity);
				if (!logEventSummaries.isEmpty()) {
					logEvenSummaryBySeverity.put(severity, logEventSummaries);
				}
			}
			if (!logEvenSummaryBySeverity.isEmpty()) {
				result.put(serverName, logEvenSummaryBySeverity);
			}
		}
		return result;
	}

	protected static class DateSeverityKey {
		private Date date;
		private String severity;

		public DateSeverityKey(Date date, String severity) {
			this.date = date;
			this.severity = severity;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((date == null) ? 0 : date.hashCode());
			result = prime * result
					+ ((severity == null) ? 0 : severity.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DateSeverityKey other = (DateSeverityKey) obj;
			if (date == null) {
				if (other.date != null)
					return false;
			} else if (!date.equals(other.date))
				return false;
			if (severity == null) {
				if (other.severity != null)
					return false;
			} else if (!severity.equals(other.severity))
				return false;
			return true;
		}
	}

	protected List<LogEventSummary> getLogEventSummaries(String serverName,
			Date fromDate, Date toDate, SummaryBy summaryBy, String severity)
			throws SQLException {
		List<LogEventSummary> result = new ArrayList<LogEventSummary>();
		List<LogEvent> logEvents = getLogEvents(serverName, fromDate, toDate,
				severity, localDbConnection);
		Map<DateSeverityKey, LogEventSummary> eventSummaries = new HashMap<DataAccess.DateSeverityKey, LogEventSummary>();
		for (LogEvent event : logEvents) {
			Date date = Utils.sqlTimestampToUtilDate(event.getTimestamp_());
			if (summaryBy == SummaryBy.HOUR) {
				date = Utils.hour(date);
			}
			if (summaryBy == SummaryBy.DAY) {
				date = Utils.date(date);
			}
			DateSeverityKey key = new DateSeverityKey(date, severity);
			LogEventSummary eventSummary = eventSummaries.get(key);
			if (eventSummary == null) {
				eventSummary = new LogEventSummary(serverName, date, severity);
				eventSummaries.put(key, eventSummary);
				result.add(eventSummary);
			}
			eventSummary.getEvents().add(event);
		}
		return result;
	}

	protected static final String LOG_TABLE_NAME = "DB2_LOG";

	protected static final String SQL_CREATE_LOG_TABLE = "create cached table "
			+ LOG_TABLE_NAME
			+ "(id identity, servername varchar(128) not null, timestamp_ timestamp, timezone integer, "
			+ "instancename varchar(128), dbpartitionnum smallint, dbname varchar(128), "
			+ "pid bigint, processname varchar(255), tid bigint, "
			+ "appl_id varchar(64), component varchar(255), function varchar(255), probe integer, "
			+ "msgnum integer, msgtype char(3), msgseverity char(1), msg varchar(16384))";

	protected static final String SQL_CREATE_LOG_INDEX = "create index "
			+ LOG_TABLE_NAME + "_svr_ts on " + LOG_TABLE_NAME
			+ "(servername, timestamp_)";

	protected boolean logTableExists(Connection con) throws SQLException {
		DatabaseMetaData dbmd = con.getMetaData();
		ResultSet rst = dbmd.getTables(null, null, LOG_TABLE_NAME,
				new String[] { "TABLE" });
		try {
			return rst.next();
		} finally {
			rst.close();
		}
	}

	protected void createLogTable(Connection con) throws SQLException {
		Statement stm = con.createStatement();
		try {
			stm.executeUpdate(SQL_CREATE_LOG_TABLE);
			stm.executeUpdate(SQL_CREATE_LOG_INDEX);
		} finally {
			stm.close();
		}
	}

	protected static String SQL_GET_MAX_TS = "select max(timestamp_) from "
			+ LOG_TABLE_NAME + " where servername = ?";

	// NOTE:
	// Function doesn't honors parameter, always returns all record
	// so I'm filtering records from resulting table.
	// This happens in DB2-expc 10.1 linux 64.
	protected static String SQL_GET_LOG_FROM_DB2 = "select "
			+ "t.timestamp , t.timezone, t.instancename, t.dbpartitionnum, t.dbname, "
			+ "t.pid, t.processname, t.tid, t.appl_id, t.component, t.function, t.probe, "
			+ "t.msgnum, t.msgtype, t.msgseverity, cast(t.msg as varchar(16384)) "
			+ "from table(pd_get_log_msgs(null)) as t where t.timestamp > ?";

	protected static String SQL_INS_LOG = "insert into "
			+ LOG_TABLE_NAME
			+ " (servername, timestamp_ ,timezone, instancename, dbpartitionnum, dbname, "
			+ "pid, processname, tid, appl_id, component, function, probe, "
			+ "msgnum, msgtype, msgseverity, msg) "
			+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	protected void getLogEventsFromDB2(String serverName, Connection srcCon,
			Connection destCon) throws SQLException {
		QueryRunner queryRunner = new QueryRunner();
		Timestamp maxTs = queryRunner.query(destCon, SQL_GET_MAX_TS,
				new ScalarHandler<Timestamp>(), serverName);
		if (maxTs == null) {
			maxTs = Utils.utilDateToSqlTimestamp(Utils
					.addMonths(new Date(), -1));
		}
		List<Object[]> logEntries = queryRunner.query(srcCon,
				SQL_GET_LOG_FROM_DB2, new ArrayListHandler(), maxTs);
		destCon.setAutoCommit(false);
		try {
			for (Object[] logEntry : logEntries) {
				Object[] entryToInsert = new Object[logEntry.length + 1];
				entryToInsert[0] = serverName;
				for (int i = 0; i < logEntry.length; i++) {
					entryToInsert[i + 1] = logEntry[i];
				}
				queryRunner.update(destCon, SQL_INS_LOG, entryToInsert);
			}
			destCon.commit();
		} catch (SQLException ex) {
			destCon.rollback();
			throw ex;
		} finally {
			destCon.setAutoCommit(true);
		}
	}

	protected static final String SQL_GET_EVENTS = "select * from db2_log where servername = ? "
			+ "and timestamp_ between ? and ? and msgseverity = ? order by timestamp_";

	protected List<LogEvent> getLogEvents(String serverName, Date fromDate,
			Date toDate, String severity, Connection con) throws SQLException {
		QueryRunner queryRunner = new QueryRunner();
		List<LogEvent> result = queryRunner.query(con, SQL_GET_EVENTS,
				new BeanListHandler<LogEvent>(LogEvent.class), serverName,
				Utils.utilDateToSqlTimestamp(fromDate),
				Utils.utilDateToSqlTimestamp(toDate), severity);
		return result;
	}
}
