package com.db2utils.dto;

import java.sql.Timestamp;

public class LogEvent {

	private long id;
	private Timestamp timestamp_;
	private int timezone;
	private String instancename;
	private int dbpartitionnum;
	private String dbname;
	private long pid;
	private String processname;
	private long tid;
	private String appl_id;
	private String component;
	private String function;
	private int probe;
	private int msgnum;
	private String msgtype;
	private String msgseverity;
	private String msg;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getTimestamp_() {
		return timestamp_;
	}

	public void setTimestamp_(Timestamp timestamp_) {
		this.timestamp_ = timestamp_;
	}

	public int getTimezone() {
		return timezone;
	}

	public void setTimezone(int timezone) {
		this.timezone = timezone;
	}

	public String getInstancename() {
		return instancename;
	}

	public void setInstancename(String instancename) {
		this.instancename = instancename;
	}

	public int getDbpartitionnum() {
		return dbpartitionnum;
	}

	public void setDbpartitionnum(int dbpartitionnum) {
		this.dbpartitionnum = dbpartitionnum;
	}

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public long getPid() {
		return pid;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	public String getProcessname() {
		return processname;
	}

	public void setProcessname(String processname) {
		this.processname = processname;
	}

	public long getTid() {
		return tid;
	}

	public void setTid(long tid) {
		this.tid = tid;
	}

	public String getAppl_id() {
		return appl_id;
	}

	public void setAppl_id(String appl_id) {
		this.appl_id = appl_id;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public int getProbe() {
		return probe;
	}

	public void setProbe(int probe) {
		this.probe = probe;
	}

	public int getMsgnum() {
		return msgnum;
	}

	public void setMsgnum(int msgnum) {
		this.msgnum = msgnum;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public String getMsgseverity() {
		return msgseverity;
	}

	public void setMsgseverity(String msgseverity) {
		this.msgseverity = msgseverity;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
