package com.db2utils.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LogEventSummary {

	public enum SummaryBy {
		HOUR, DAY
	}

	private String serverName;
	private Date date;
	private String severity;
	private List<LogEvent> events = new ArrayList<LogEvent>();

	public LogEventSummary(String serverName, Date date, String severity) {
		this.serverName = serverName;
		this.date = date;
		this.severity = severity;
	}

	public String getServerName() {
		return serverName;
	}

	public Date getDate() {
		return date;
	}

	public String getSeverity() {
		return severity;
	}

	public int getCount() {
		return events.size();
	}

	public List<LogEvent> getEvents() {
		return events;
	}

}
