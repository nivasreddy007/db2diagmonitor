package com.db2utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Utils {

	public static Date addMonths(Date date, int months) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, months);
		return cal.getTime();
	}

	public static Date addDays(Date date, int days) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, days);
		return cal.getTime();
	}

	public static Date addHours(Date date, int hours) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, hours);
		return cal.getTime();
	}

	public static Date date(Date date) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date hour(Date date) {
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date sqlTimestampToUtilDate(java.sql.Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}
		return new Date(timestamp.getTime());
	}

	public static java.sql.Timestamp utilDateToSqlTimestamp(Date date) {
		if (date == null) {
			return null;
		}
		java.sql.Timestamp result = new java.sql.Timestamp(date.getTime());
		return result;
	}

	public static List<String> splitText(String text, String wordSeparators,
			int maxLength) {
		if (wordSeparators == null) {
			wordSeparators = " ";
		}
		List<String> result = new ArrayList<String>();
		String[] lines = text.split("\\n");
		for (String line : lines) {
			if (line.length() > maxLength) {
				int fromPos = 0, prevPos = 0;
				while (line.length() - fromPos > maxLength) {
					int toPos = fromPos + maxLength;
					if (toPos > line.length()) {
						toPos = line.length() - 1;
					}
					while (toPos > 0
							&& wordSeparators.indexOf(text.charAt(toPos)) == -1) {
						toPos--;
					}
					if (toPos == prevPos) {
						toPos = fromPos + maxLength;
					}
					result.add(line.substring(fromPos, toPos));
					prevPos = fromPos = toPos;
				}
				result.add(line.substring(fromPos));
			} else {
				result.add(line);
			}
		}
		return result;
	}

	public static String prepareExeceptionMessage(Exception exception) {
		if (exception == null) {
			return null;
		}
		String msg = exception.getMessage();
		if (isEmpty(msg)) {
			msg = exception.getClass().getSimpleName();
		}
		StringBuilder sb = new StringBuilder();
		List<String> lines = Utils.splitText(msg, " ,;.\t", 50);
		for (String line : lines) {
			sb.append(line);
			sb.append('\n');
		}
		return sb.toString();
	}

	public static boolean isEmpty(String value) {
		return value == null || "".equals(value.trim());
	}
}
